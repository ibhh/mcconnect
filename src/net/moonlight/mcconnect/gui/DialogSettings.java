/*
 * Copyright (c) 2015 Felix Beil
 * 
 * This file is part of McConnect.
 * 
 * McConnect is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * McConnect is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with McConnect. If not, see <http://www.gnu.org/licenses/>.
 */
package net.moonlight.mcconnect.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.WindowConstants;

/**
 * Shows all program settings.
 */
public class DialogSettings extends JDialog
{
	private static final long serialVersionUID = 1L;

	public DialogSettings(MainFrame parent)
	{
		setTitle("Settings");
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setModal(true);
		setLayout(new BorderLayout());
		setSize(new Dimension(400, 250));
		addComponents();
		setLocationRelativeTo(parent);
		setVisible(true);
	}

	/**
	 * Adds all visual components to the frame.
	 */
	private void addComponents()
	{
		JTabbedPane tabbedPane = new JTabbedPane();

		JPanel panelGeneral = new JPanel();
		tabbedPane.addTab("General", panelGeneral);

		add(tabbedPane);
	}
}
