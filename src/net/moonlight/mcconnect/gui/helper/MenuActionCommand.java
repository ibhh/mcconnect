/*
 * Copyright (c) 2015 Felix Beil
 * 
 * This file is part of McConnect.
 * 
 * McConnect is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * McConnect is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with McConnect. If not, see <http://www.gnu.org/licenses/>.
 */
package net.moonlight.mcconnect.gui.helper;

import java.io.IOException;

import net.moonlight.mcconnect.gui.MainFrame;

/**
 * Executes a command on action.
 */
public class MenuActionCommand implements MenuAction
{
	private MainFrame mainFrame;
	private String command;

	public MenuActionCommand(MainFrame mainFrame, String command)
	{
		this.mainFrame = mainFrame;
		this.command = command;
	}

	@Override
	public void execute()
	{
		try
		{
			mainFrame.getConnection().command(command);
		}
		catch(IOException exception)
		{
			// TODO notify user
			exception.printStackTrace();
		}
	}
}
