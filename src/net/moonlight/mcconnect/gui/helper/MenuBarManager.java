/*
 * Copyright (c) 2015 Felix Beil
 * 
 * This file is part of McConnect.
 * 
 * McConnect is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * McConnect is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with McConnect. If not, see <http://www.gnu.org/licenses/>.
 */
package net.moonlight.mcconnect.gui.helper;

import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import net.moonlight.mcconnect.gui.DialogBuilder;
import net.moonlight.mcconnect.gui.MainFrame;
import net.moonlight.mcconnect.main.MCConnect;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Manages the MenuBar for the GUI.
 */
public class MenuBarManager
{
	/**
	 * Maps the menu item name to the menu item to acces them later, e.g. enable or disable a specific menu item.
	 */
	private HashMap<String, JMenuItem> itemMap;
	/**
	 * Contains all loaded lists that can be used in a parameter of type select.
	 */
	private HashMap<String, String[][]> listsMap;
	private MenuActionListener mActionListener;
	private MainFrame mainFrame;

	public MenuBarManager(MainFrame mainFrame)
	{
		this.mainFrame = mainFrame;
		itemMap = new HashMap<String, JMenuItem>();
		listsMap = new HashMap<String, String[][]>();
		mActionListener = new MenuActionListener(mainFrame, this);
	}

	/**
	 * Builds a single menu from the given json object. Also adds all related menu items and sub menus.
	 * @param key The internal key of this menu.
	 * @param jsonObject The json object, that describes the menu.
	 * @return The JMenu build from the json object.
	 */
	private JMenu buildJsonMenu(String key, JSONObject jsonObject)
	{
		StringBuilder sb = new StringBuilder();
		// Add spaces to add some spacing in the menu bar
		sb.append(" ");
		sb.append(jsonObject.get("name"));
		sb.append(" ");
		JMenu menu = new JMenu(sb.toString());
		if(jsonObject.containsKey("mnemonic"))
		{
			try
			{
				menu.setMnemonic(KeyEvent.getExtendedKeyCodeForChar(((String) jsonObject.get("mnemonic")).toCharArray()[0]));
			}
			catch(Exception exception)
			{
				exception.printStackTrace();
			}
		}
		JSONArray itemsArray = (JSONArray) jsonObject.get("items");
		for(int i = 0; i < itemsArray.size(); i++)
		{
			try
			{
				buildJsonMenuItem(menu, key, (JSONObject) itemsArray.get(i));
			}
			catch(ClassCastException exception)
			{
				MCConnect.logGui("Failed to load menu item #" + i + " in menu '" + key + "'!");
			}
		}

		return menu;
	}

	/**
	 * Creates a menu item from the given json object and adds it to the given menu.<br />
	 * <i>Note:</i><br />
	 * The menu item can also be a (sub) menu.
	 * @param menu The menu to add the menu item to.
	 * @param key The internal key of this menu.
	 * @param jsonObject The json object, that describes the menu item.
	 */
	private void buildJsonMenuItem(JMenu menu, String key, JSONObject jsonObject)
	{
		if(!jsonObject.containsKey("type"))
		{
			MCConnect.logGui("Missing 'type' in menu item of '" + key + "'!");
			return;
		}
		String type = (String) jsonObject.get("type");
		switch(type)
		{
			case "item":
				JMenuItem menuItem = new JMenuItem((String) jsonObject.get("name"));
				if(!jsonObject.containsKey("id"))
				{
					MCConnect.logGui("Missing 'id' in menu item of menu '" + key + "'!");
					return;
				}
				// append id to get a new key for this menu item
				key += ".";
				key += (String) jsonObject.get("id");
				// Parse command that should be executed
				if(!jsonObject.containsKey("command"))
				{
					MCConnect.logGui("Useless menu item! Missing 'command' in '" + key + "'!");
					return;
				}
				String command = (String) jsonObject.get("command");
				// Parse command parameters
				if(jsonObject.containsKey("parameters"))
				{
					DialogBuilder dialogBuilder = new DialogBuilder();
					dialogBuilder.setParent(mainFrame);
					dialogBuilder.setCommand(command);
					dialogBuilder.setTitle(key);
					JSONObject parameters = (JSONObject) jsonObject.get("parameters");
					@SuppressWarnings("unchecked")
					Iterator<String> paramIterator = parameters.keySet().iterator();
					while(paramIterator.hasNext())
					{
						// Parse single parameter
						String paramKey = paramIterator.next();
						JSONObject paramObject = (JSONObject) parameters.get(paramKey);
						if(!paramObject.containsKey("type"))
						{
							MCConnect.logGui("Missing 'type' on command parameter in '" + key + "#" + paramKey + "'!");
							continue;
						}
						// Parse whether parameter is optional or not, default is optional.
						boolean optional = false;
						if(paramObject.containsKey("optional"))
						{
							optional = (boolean) paramObject.get("optional");
						}
						// Parse command description
						String description = "";
						if(paramObject.containsKey("description"))
						{
							description = (String) paramObject.get("description");
						}
						// Parse additional values based on parameter type
						switch((String) paramObject.get("type"))
						{
							case "text":
								dialogBuilder.addTextParameter(paramKey, description, optional);
								break;
							case "select":
								if(!paramObject.containsKey("values"))
								{
									MCConnect.logGui("Missing 'values' in command parameter type 'select' in menu item '" + key + "#" + paramKey + "'!");
									break;
								}
								// Parse all possible values
								Object selectValues = paramObject.get("values");
								String[][] list = new String[0][0];
								// selectValues can either be a String, which referes to a previously defined list, or an array
// with name value pairs
								if(selectValues instanceof String)
								{
									String listName = (String) selectValues;
									if(!listsMap.containsKey(listName))
									{
										MCConnect.logGui("List '" + listName + "' was not found in menu item '" + key + "#" + paramKey + "'!");
										break;
									}
									list = listsMap.get(listName);
								}
								else
								{
									list = parseList((JSONArray) selectValues);
								}
								dialogBuilder.addSelectParameter(paramKey, description, list[0], list[1], optional);
								break;
							case "player":
								// TODO update player list, add select parameter
								MCConnect.logGui("Parameter type 'player' is not yet supported.");
								break;
							case "number":
								int min = Integer.MIN_VALUE;
								int max = Integer.MAX_VALUE;
								int step = 1;
								if(paramObject.containsKey("minVal"))
								{
									min = Integer.valueOf(String.valueOf(paramObject.get("minVal")));
								}
								if(paramObject.containsKey("maxVal"))
								{
									max = Integer.valueOf(String.valueOf(paramObject.get("maxVal")));
								}
								if(paramObject.containsKey("step"))
								{
									step = Integer.valueOf(String.valueOf(paramObject.get("step")));
								}
								dialogBuilder.addNumberParameter(paramKey, description, min, max, step, optional);
								break;
							case "coordinates":
								dialogBuilder.addCoordinatesParameter(paramKey, description, optional);
								break;
							default:
								MCConnect.logGui("Unknown command parameter type in '" + key + "#" + paramKey + "'!");
						}
					}
					// Show dialog with parameters on action
					mActionListener.registerActionCommand(key, new MenuActionDialog(dialogBuilder));
				}
				else
				{
					// Send command on action if no parameters
					mActionListener.registerActionCommand(key, new MenuActionCommand(mainFrame, command));
				}
				menuItem.setActionCommand(key);
				menuItem.addActionListener(mActionListener);
				// Parse short cut is present
				if(jsonObject.containsKey("mnemonic"))
				{
					try
					{
						menuItem.setMnemonic(KeyEvent.getExtendedKeyCodeForChar(((String) jsonObject.get("mnemonic")).toCharArray()[0]));
					}
					catch(Exception exception)
					{
						exception.printStackTrace();
					}
				}
				menu.add(menuItem);
				itemMap.put(key, menuItem);
				break;
			case "menu":
				// TODO add type menu
				MCConnect.logGui("Type 'menu' is not yet implemented.");
				break;
			case "separator":
				menu.addSeparator();
				break;
			default:
				MCConnect.logGui("Undefined menu item type: " + type);
				break;
		}
	}

	/**
	 * Loads the commands.json and parses all menus from it.
	 * @return An array of menus that have been parsed from the json file.
	 */
	private JMenu[] buildJsonMenus()
	{
		ArrayList<JMenu> menusList = new ArrayList<JMenu>();
		JSONParser parser = new JSONParser();
		Object obj = null;

		try
		{
			obj = parser.parse(new FileReader(new File("commands.json")));
		}
		catch(FileNotFoundException exception)
		{
			MCConnect.logGui("Failed to load commands! (file not found)");
			exception.printStackTrace();
		}
		catch(IOException exception)
		{
			MCConnect.logGui("Failed to load commands! (error while reading)");
			exception.printStackTrace();
		}
		catch(ParseException exception)
		{
			MCConnect.logGui("Failed to load commands! (could not parse file)");
			exception.printStackTrace();
		}

		if(obj != null)
		{
			JSONObject rootObject = null;
			try
			{
				rootObject = (JSONObject) obj;
			}
			catch(ClassCastException exception)
			{
				MCConnect.logGui("Failed to load commands! (could read root object)");
				exception.printStackTrace();
			}

			if(rootObject != null)
			{
				if(!rootObject.containsKey("lists"))
				{
					MCConnect.logGui("Failed to load commands! (missing object 'lists')");
				}
				else if(!rootObject.containsKey("menus"))
				{
					MCConnect.logGui("Failed to load commands! (missing object 'menus')");
				}
				else
				{
					// load lists
					listsMap = new HashMap<String, String[][]>();
					JSONObject listObject = (JSONObject) rootObject.get("lists");
					@SuppressWarnings("unchecked")
					Iterator<String> iteratorLists = listObject.keySet().iterator();
					while(iteratorLists.hasNext())
					{
						String key = iteratorLists.next();
						try
						{
							listsMap.put(key, parseList((JSONArray) listObject.get(key)));
						}
						catch(Exception exception)
						{
							MCConnect.logGui("Failed to load list '" + key + "'");
						}
					}

					// load menus
					JSONObject menusObject = (JSONObject) rootObject.get("menus");
					@SuppressWarnings("unchecked")
					Iterator<String> iteratorMenus = menusObject.keySet().iterator();
					while(iteratorMenus.hasNext())
					{
						String key = iteratorMenus.next();
						try
						{
							JMenu menu = buildJsonMenu(key, (JSONObject) menusObject.get(key));
							menusList.add(menu);
							key = checkMenuKey(key);
							itemMap.put(key, menu);
						}
						catch(Exception exception)
						{
							MCConnect.logGui("Failed to load menu '" + key + "'");
						}
					}
				}
			}
		}
		return menusList.toArray(new JMenu[0]);
	}

	/**
	 * Creates the menu bar and stores all references.
	 * @return The menu bar.
	 */
	public JMenuBar buildMenuBar()
	{
		JMenuBar menuBar = new JMenuBar();

		// Build all static menus first
		JMenu menuConnection = buildMenuConnection();
		JMenu menuInfo = buildMenuInfo();

		menuBar.add(menuConnection);
		// Build and add menus form commands.json
		for(JMenu jsonMenu : buildJsonMenus())
		{
			menuBar.add(jsonMenu);
		}
		menuBar.add(menuInfo);

		System.out.println("Found " + itemMap.size() + " menus and menu items.");
		return menuBar;
	}

	/**
	 * Create the "Connect" menu.
	 * @return The created menu.
	 */
	private JMenu buildMenuConnection()
	{
		String key = "connect";
		JMenu menuConnect = new JMenu(" Connect ");
		menuConnect.setMnemonic(KeyEvent.VK_C);
		itemMap.put(key, menuConnect);

		key = "connect.to";
		JMenuItem menuItemConnectTo = new JMenuItem("Connect To ...");
		menuItemConnectTo.setMnemonic(KeyEvent.VK_T);
		menuItemConnectTo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, InputEvent.CTRL_DOWN_MASK));
		menuItemConnectTo.setActionCommand(key);
		menuItemConnectTo.addActionListener(mActionListener);
		menuConnect.add(menuItemConnectTo);
		itemMap.put(key, menuItemConnectTo);

		key = "connect.disconnect";
		JMenuItem menuItemDisconnect = new JMenuItem("Disconnect");
		menuItemDisconnect.setMnemonic(KeyEvent.VK_D);
		menuItemDisconnect.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_DOWN_MASK));
		menuItemDisconnect.setActionCommand(key);
		menuItemDisconnect.setEnabled(false);
		menuItemDisconnect.addActionListener(mActionListener);
		menuConnect.add(menuItemDisconnect);
		itemMap.put(key, menuItemDisconnect);

		menuConnect.addSeparator();

		key = "connect.saved";
		JMenu menuSavedConnections = new JMenu("Saved Connections");
		menuSavedConnections.setMnemonic(KeyEvent.VK_S);
		menuConnect.add(menuSavedConnections);
		itemMap.put(key, menuSavedConnections);

		key = "connect.manager";
		JMenuItem menuItemConnectionManager = new JMenuItem("Connection Manager");
		menuItemConnectionManager.setMnemonic(KeyEvent.VK_M);
		menuItemConnectionManager.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_DOWN_MASK));
		menuItemConnectionManager.setActionCommand(key);
		menuItemConnectionManager.addActionListener(mActionListener);
		menuConnect.add(menuItemConnectionManager);
		itemMap.put(key, menuItemConnectionManager);

		menuConnect.addSeparator();

		key = "connect.exit";
		JMenuItem menuItemExit = new JMenuItem("Exit");
		menuItemExit.setMnemonic(KeyEvent.VK_E);
		menuItemExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, InputEvent.ALT_DOWN_MASK));
		menuItemExit.setActionCommand(key);
		menuItemExit.addActionListener(mActionListener);
		menuConnect.add(menuItemExit);
		itemMap.put(key, menuItemExit);

		return menuConnect;
	}

	/**
	 * Create the "Info" menu.
	 * @return The created menu.
	 */
	private JMenu buildMenuInfo()
	{
		String key = "info";
		JMenu menuInfo = new JMenu(" Info ");
		menuInfo.setMnemonic(KeyEvent.VK_I);
		itemMap.put(key, menuInfo);

		key = "info.about";
		JMenuItem menuItemAbout = new JMenuItem("About");
		menuItemAbout.setMnemonic(KeyEvent.VK_A);
		menuItemAbout.setActionCommand(key);
		menuItemAbout.addActionListener(mActionListener);
		menuInfo.add(menuItemAbout);
		itemMap.put(key, menuItemAbout);

		key = "info.settings";
		JMenuItem menuItemSettings = new JMenuItem("Settings");
		menuItemSettings.setMnemonic(KeyEvent.VK_S);
		menuItemSettings.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_COMMA, InputEvent.CTRL_DOWN_MASK));
		menuItemSettings.setActionCommand(key);
		menuItemSettings.addActionListener(mActionListener);
		menuInfo.add(menuItemSettings);
		itemMap.put(key, menuItemSettings);

		return menuInfo;
	}

	/**
	 * Checks if the key is used for another menu already and changes it to an yet unused key by appending '-'.<br />
	 * <i>Note:</i><br />
	 * Only keys for top level menus have to be checked, since keys for sub menus are built by prepending the key of the parent
	 * menu.
	 * @param key The key that has to be checked.
	 * @return A valid key for a menu.
	 */
	private String checkMenuKey(String key)
	{
		while(itemMap.containsKey(key))
		{
			key += "-";
		}
		return key;
	}

	public JMenuItem getMenuItem(String key)
	{
		return itemMap.get(key);
	}

	/**
	 * Will be called after a connection has been made.
	 */
	public void onConnect()
	{
		getMenuItem("connect.disconnect").setEnabled(true);
		// TODO enable json menus (disabled due to testing)
	}

	/**
	 * Will be called after a connection has been stoped.
	 */
	public void onDisconnect()
	{
		getMenuItem("connect.disconnect").setEnabled(false);
		// TODO disable json menus (disabled due to testing)
	}

	/**
	 * Parses a list form an json array into two array containing the name value pairs.
	 * @param jsonArray The json array to parse from.
	 * @return The parsed list. The first array is an array of size 2, index 0 contains an array of all names and index 1
	 *         contains an array of all values.
	 */
	private String[][] parseList(JSONArray jsonArray)
	{
		String[][] list = new String[2][jsonArray.size()];
		for(int i = 0; i < jsonArray.size(); i++)
		{
			try
			{
				JSONObject jsonObject = (JSONObject) jsonArray.get(i);
				list[0][i] = (String) jsonObject.get("name");
				list[1][i] = (String) jsonObject.get("value");
			}
			catch(ClassCastException exception)
			{
				MCConnect.logGui("Failed to load list item #" + i);
			}
		}
		return list;
	}
}
