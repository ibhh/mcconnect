/*
 * Copyright (c) 2015 Felix Beil
 * 
 * This file is part of McConnect.
 * 
 * McConnect is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * McConnect is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with McConnect. If not, see <http://www.gnu.org/licenses/>.
 */
package net.moonlight.mcconnect.gui.helper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.LogRecord;

/**
 * Formats the log messages to fit the users preferences.
 */
public class Formatter extends java.util.logging.Formatter
{
	private String dateFormat;

	/**
	 * Use the default date format of HH:mm.
	 */
	public Formatter()
	{
		this("HH:mm");
	}

	/**
	 * @param dateFormat The format to be used for the time stamp of the log message.
	 */
	public Formatter(String dateFormat)
	{
		this.dateFormat = dateFormat;
	}

	@Override
	public String format(LogRecord record)
	{
		StringBuilder builder = new StringBuilder();
		if(dateFormat != null && dateFormat != "")
		{
			SimpleDateFormat dateFormater = new SimpleDateFormat(dateFormat);
			builder.append(dateFormater.format(new Date(record.getMillis())));
			builder.append("  ");
		}
		builder.append(formatMessage(record));
		builder.append(System.getProperty("line.separator"));
		return builder.toString();
	}
}
