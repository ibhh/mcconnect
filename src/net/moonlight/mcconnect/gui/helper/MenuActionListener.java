/*
 * Copyright (c) 2015 Felix Beil
 * 
 * This file is part of McConnect.
 * 
 * McConnect is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * McConnect is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with McConnect. If not, see <http://www.gnu.org/licenses/>.
 */
package net.moonlight.mcconnect.gui.helper;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import net.moonlight.mcconnect.gui.DialogConnectTo;
import net.moonlight.mcconnect.gui.DialogSettings;
import net.moonlight.mcconnect.gui.MainFrame;
import net.moonlight.mcconnect.main.MCConnect;

/**
 * Listens for all menu actions. Any registered menu item should have an action command, based on which the MenuActionListener
 * performs its actions.
 */
public class MenuActionListener implements ActionListener
{
	private MCConnect mcConnect;
	private MenuBarManager menuBarMngr;
	private MainFrame mainFrame;
	private HashMap<String, MenuAction> registeredCommands;

	public MenuActionListener(MainFrame mainFrame, MenuBarManager menuBarManager)
	{
		this.mainFrame = mainFrame;
		this.menuBarMngr = menuBarManager;
		mcConnect = this.mainFrame.getConnection();
		registeredCommands = new HashMap<String, MenuAction>();
	}

	@Override
	public void actionPerformed(ActionEvent event)
	{
		switch(event.getActionCommand())
		{
			case "connect.to":
				new DialogConnectTo(mainFrame);
				break;
			case "connect.disconnect":
				mcConnect.disconnect();
				menuBarMngr.onDisconnect();
				break;
			case "connect.exit":
				mcConnect.disconnect();
				System.exit(0);
				break;
			case "info.settings":
				new DialogSettings(mainFrame);
				break;
			default:
				if(registeredCommands.containsKey(event.getActionCommand()))
				{
					registeredCommands.get(event.getActionCommand()).execute();
					break;
				}
				System.out.println("Call to unknown action command: " + event.getActionCommand());
				break;
		}
	}

	/**
	 * Register a new action to listen for.
	 * @param cmd The action command to listen for.
	 * @param menuAction The action that should be executed, when the action command was triggered.
	 */
	public void registerActionCommand(String cmd, MenuAction menuAction)
	{
		registeredCommands.put(cmd, menuAction);
	}
}
