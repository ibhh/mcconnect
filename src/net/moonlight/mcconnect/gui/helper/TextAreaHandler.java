/*
 * Copyright (c) 2015 Felix Beil
 * 
 * This file is part of McConnect.
 * 
 * McConnect is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * McConnect is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with McConnect. If not, see <http://www.gnu.org/licenses/>.
 */
package net.moonlight.mcconnect.gui.helper;

import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

import javax.swing.JTextArea;

/**
 * A handler for the logger that displays any log messages in the text area.
 */
public class TextAreaHandler extends Handler
{
	private JTextArea textArea;

	public TextAreaHandler(JTextArea textArea)
	{
		this.textArea = textArea;
	}

	@Override
	public void close() throws SecurityException
	{

	}

	@Override
	public void flush()
	{

	}

	@Override
	public void publish(LogRecord record)
	{
		Formatter formatter = getFormatter();
		if(formatter != null)
		{
			textArea.append(formatter.format(record));
		}
		else
		{
			textArea.append(record.getMessage());
		}
	}
}
