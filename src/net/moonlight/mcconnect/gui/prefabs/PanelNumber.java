/*
 * Copyright (c) 2015 Felix Beil
 * 
 * This file is part of McConnect.
 * 
 * McConnect is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * McConnect is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with McConnect. If not, see <http://www.gnu.org/licenses/>.
 */
package net.moonlight.mcconnect.gui.prefabs;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

/**
 * A parameter of type number.
 */
public class PanelNumber extends ParameterPanel
{
	private static final long serialVersionUID = 1L;
	private JCheckBox checkBoxEnabled;
	private JSpinner spinnerNumber;
	private final int minValue;

	public PanelNumber(String name, String description, int min, int max, int step, boolean optional)
	{
		super(name, optional);
		minValue = min;
		setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();
		int totalHeight = 0;

		if(isOptional)
		{
			checkBoxEnabled = new JCheckBox();
			checkBoxEnabled.addChangeListener(event ->
			{
				isOptionalEnabled = ((JCheckBox) event.getSource()).isSelected();
				spinnerNumber.setEnabled(isOptionalEnabled);
			});
			c.gridx = 0;
			c.gridy = 0;
			c.weightx = 0.0f;
			c.weighty = 0.0f;
			c.fill = GridBagConstraints.NONE;
			c.anchor = GridBagConstraints.FIRST_LINE_START;
			c.insets = new Insets(0, 0, 0, 2);
			add(checkBoxEnabled, c);
			totalHeight += checkBoxEnabled.getPreferredSize().height;
		}

		JLabel labelText = new JLabel(description + ":");
		c.gridx = optional ? 1 : 0;
		c.gridy = 0;
		c.weightx = 1.0f;
		c.weighty = 1.0f;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.CENTER;
		c.insets = new Insets(0, 0, 0, 0);
		add(labelText, c);
		totalHeight = Math.max(totalHeight, labelText.getPreferredSize().height);

		spinnerNumber = new JSpinner(new SpinnerNumberModel(Math.max(min, 0), min, max, step));
		spinnerNumber.setEnabled(!optional);
		c.gridx = optional ? 1 : 0;
		c.gridy = 1;
		c.weightx = 1.0f;
		c.weighty = 1.0f;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.CENTER;
		c.insets = new Insets(2, 0, 0, 0);
		add(spinnerNumber, c);
		totalHeight += spinnerNumber.getPreferredSize().height;

		setPreferredSize(new Dimension(290, totalHeight + 2));
	}

	@Override
	public void clear()
	{
		spinnerNumber.setValue(Math.max(minValue, 0));
	}

	@Override
	public String getValue()
	{
		return String.valueOf(spinnerNumber.getValue());
	}
}
