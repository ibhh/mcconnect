/*
 * Copyright (c) 2015 Felix Beil
 * 
 * This file is part of McConnect.
 * 
 * McConnect is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * McConnect is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with McConnect. If not, see <http://www.gnu.org/licenses/>.
 */
package net.moonlight.mcconnect.gui.prefabs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JDialog;

import net.moonlight.mcconnect.gui.MainFrame;

/**
 * The dialog that shows up for each command.
 */
public class CommandDialog extends JDialog
{
	private static final long serialVersionUID = 1L;
	private String cmd;
	private MainFrame mainFrame;
	private ArrayList<ParameterPanel> components;

	public CommandDialog(MainFrame parent, String cmd)
	{
		super(parent);
		mainFrame = parent;
		components = new ArrayList<ParameterPanel>();
		this.cmd = cmd;
	}

	/**
	 * Adds a command parameter to the dialog.
	 * @param parameter The panel that contains all necessary components for the command parameter.
	 */
	public void addParameter(ParameterPanel parameter)
	{
		components.add(parameter);
	}

	/**
	 * @return The command that will be send, when the user clicks the send button. The parameters are not replaced in this
	 *         string.
	 */
	public String getCommand()
	{
		return cmd;
	}

	public void onCancel()
	{
		dispose();
	}

	public void onSend()
	{
		/**
		 * Contains all data of replaced parameters
		 */
		HashMap<String, String> paramToValue = new HashMap<String, String>();
		String cmd = getCommand();
		for(ParameterPanel pp : components)
		{
			cmd = cmd.replace("{" + pp.getName() + "}", pp.getValue());
			paramToValue.put(pp.getName(), pp.getValue());
		}

		try
		{
			mainFrame.getConnection().command(cmd);
		}
		catch(IOException exception)
		{
			// TODO Notify user
			exception.printStackTrace();
		}
		dispose();
	}
}
