/*
 * Copyright (c) 2015 Felix Beil
 * 
 * This file is part of McConnect.
 * 
 * McConnect is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * McConnect is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with McConnect. If not, see <http://www.gnu.org/licenses/>.
 */
package net.moonlight.mcconnect.gui;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.logging.Handler;
import java.util.logging.Level;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import net.moonlight.mcconnect.gui.helper.Formatter;
import net.moonlight.mcconnect.gui.helper.MenuBarManager;
import net.moonlight.mcconnect.gui.helper.TextAreaHandler;
import net.moonlight.mcconnect.gui.helper.WindowListener;
import net.moonlight.mcconnect.main.MCConnect;

/**
 * The main window of McConnect.
 */
public class MainFrame extends JFrame
{
	private static final long serialVersionUID = 1L;
	private MCConnect mcConnect;
	private JTextField textFieldCommand;
	private JTextArea textAreaOutput;
	private JButton buttonSend;
	private ArrayList<String> cmdHistory;
	private int cmdHistoryPointer;
	private MenuBarManager menuBarMngr;

	public MainFrame()
	{
		mcConnect = new MCConnect();
		cmdHistory = new ArrayList<String>();
		menuBarMngr = new MenuBarManager(this);

		// Frame setup
		setTitle("MCConnect v" + MCConnect.getVersion());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addWindowListener(new WindowListener(mcConnect));
		setLayout(new GridBagLayout());
		setMinimumSize(new Dimension(400, 300));
		setSize(new Dimension(600, 400));
		addComponents();

		Handler handlerTexArea = new TextAreaHandler(textAreaOutput);
		handlerTexArea.setFormatter(new Formatter("HH:mm"));
		MCConnect.logger.addHandler(handlerTexArea);

		setJMenuBar(menuBarMngr.buildMenuBar());
		setLocationRelativeTo(null);
		setIconImage(new ImageIcon(getClass().getResource("/favicon.png")).getImage());
		setVisible(true);

		MCConnect.logger.log(Level.parse(MCConnect.LOG_LEVEL_GUI), "MCConnect v" + MCConnect.getVersion());
	}

	/**
	 * Adds all visual components to the frame.
	 */
	private void addComponents()
	{
		GridBagConstraints c = new GridBagConstraints();

		textAreaOutput = new JTextArea();
		textAreaOutput.setFont(new Font("Monospaced", Font.PLAIN, 12));
		textAreaOutput.setEditable(false);
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 2;
		c.weightx = 1.0f;
		c.weighty = 1.0f;
		c.fill = GridBagConstraints.BOTH;
		add(new JScrollPane(textAreaOutput, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED), c);

		textFieldCommand = new JTextField();
		textFieldCommand.setFont(new Font("Monospaced", Font.PLAIN, 12));
		textFieldCommand.requestFocus();
		textFieldCommand.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyReleased(KeyEvent event)
			{
				switch(event.getKeyCode())
				{
					case KeyEvent.VK_UP:
						if(cmdHistoryPointer > 0)
						{
							cmdHistoryPointer--;
							textFieldCommand.setText(cmdHistory.get(cmdHistoryPointer));
						}
						break;
					case KeyEvent.VK_DOWN:
						if(cmdHistoryPointer < cmdHistory.size() - 1)
						{
							cmdHistoryPointer++;
							textFieldCommand.setText(cmdHistory.get(cmdHistoryPointer));
						}
						else if(cmdHistoryPointer < cmdHistory.size())
						{
							cmdHistoryPointer++;
							textFieldCommand.setText("");
						}
						break;
				}
			}
		});
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 1;
		c.weightx = 1.0f;
		c.weighty = 0.0f;
		c.fill = GridBagConstraints.BOTH;
		add(textFieldCommand, c);

		buttonSend = new JButton("Send");
		buttonSend.addActionListener(event ->
		{
			sendCmdInput();
		});
		buttonSend.setEnabled(false);
		c.gridx = 1;
		c.gridy = 1;
		c.gridwidth = 1;
		c.weightx = 0.0f;
		c.weighty = 0.0f;
		c.fill = GridBagConstraints.BOTH;
		add(buttonSend, c);

		getRootPane().setDefaultButton(buttonSend);
	}

	/**
	 * Tries to connect to the given server and port with the given password. If the connection fails, a message dialog will be
	 * displayed informing the user about the failure.
	 * @param server The host name of the server to connect to.
	 * @param port The port to connect to.
	 * @param password The password to connect with.
	 */
	public void connect(String server, int port, char[] password)
	{
		try
		{
			mcConnect.connect(server, port);
		}
		catch(UnknownHostException exception)
		{
			exception.printStackTrace();
			JOptionPane.showMessageDialog(this, "Host not found!\n" + exception.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
		catch(IOException exception)
		{
			exception.printStackTrace();
			JOptionPane.showMessageDialog(this, "Failed to connect to server!\n" + exception.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}

		try
		{
			if(mcConnect.login(String.valueOf(password)))
			{
				buttonSend.setEnabled(true);
				menuBarMngr.onConnect();
				MCConnect.logger.log(Level.parse(MCConnect.LOG_LEVEL_GUI), "Connected to '" + server + "'");
			}
			else
			{
				JOptionPane.showMessageDialog(this, "Failed login, incorrect pssword!", "Login failed", JOptionPane.ERROR_MESSAGE);
				buttonSend.setEnabled(false);
				new DialogConnectTo(this, server, port);
			}
		}
		catch(IOException exception)
		{
			exception.printStackTrace();
			mcConnect.disconnect();
		}
	}

	/**
	 * The connection to the Minecraft server.
	 * @return The connection to the Minecraft server.
	 */
	public MCConnect getConnection()
	{
		return mcConnect;
	}

	/**
	 * Sends the command entered in the input field to the server.
	 */
	private void sendCmdInput()
	{
		try
		{
			String cmd = textFieldCommand.getText();
			cmdHistory.add(cmd);
			cmdHistoryPointer = cmdHistory.size();
			String serverOutput = mcConnect.command(cmd);
			if(serverOutput.equals(""))
			{
				serverOutput = "Command executed, no server output.";
			}
			MCConnect.logger.log(Level.parse(MCConnect.LOG_LEVEL_SERVER), serverOutput);
		}
		catch(IOException exception)
		{
			exception.printStackTrace();
			JOptionPane.showMessageDialog(this, "Failed send command!\n" + exception.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
		textFieldCommand.setText("");
	}
}
