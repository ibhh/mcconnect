/*
 * Copyright (c) 2015 Felix Beil
 * 
 * This file is part of McConnect.
 * 
 * McConnect is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * McConnect is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with McConnect. If not, see <http://www.gnu.org/licenses/>.
 */
package net.moonlight.mcconnect.localio;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import net.moonlight.mcconnect.main.MCConnect;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 * Loads and saves all settings.
 */
public class SettingsManager
{
	public static final String CONFIG_FILE = "config.xml";

	public SettingsManager()
	{

	}

	public boolean loadConfig()
	{
		File configFile = new File(CONFIG_FILE);
		if(!configFile.exists())
		{
			useDefaults();
			return false;
		}

		Document doc = null;
		try
		{
			doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new File(CONFIG_FILE));
		}
		catch(IOException exception)
		{
			MCConnect.logGui("Failed to load configuration!");
			exception.printStackTrace();
			return false;
		}
		catch(SAXException | ParserConfigurationException exception)
		{
			MCConnect.logGui("Failed to parse configuration file!");
			exception.printStackTrace();
		}

		// Check if root node exists
		if(!doc.hasChildNodes())
		{
			MCConnect.logGui("Configuration file is corrupted! (document is empty)");
			return false;
		}

		// Check root node name
		Node rootNode = doc.getFirstChild();
		if(!"config".equals(rootNode.getNodeName()))
		{
			MCConnect.logGui("Configuration file is corrupted! (root node is not 'config')");
			return false;
		}

		// Check if root node has attributers
		if(!rootNode.hasAttributes())
		{
			MCConnect.logGui("Configuration file is corrupted! (missing version attribute on root node)");
			return false;
		}
		Node rootNodeVersion = rootNode.getAttributes().getNamedItem("version");

		// Check config version
		int configVersion = 0;
		try
		{
			configVersion = Integer.parseInt(rootNodeVersion.getNodeValue());
		}
		catch(NumberFormatException exception)
		{
			MCConnect.logGui("Configuration file is corrupted! (invalid file version)");
			exception.printStackTrace();
			return false;
		}

		switch(configVersion)
		{
			case 1:
				return loadNodesV1(rootNode);
			default:
				MCConnect.logGui("Configuration file is corrupted! (unsupported file version)");
				return false;
		}
	}

	private boolean loadNodesV1(Node rootNode)
	{
		return false;
	}

	private void useDefaults()
	{

	}
}
