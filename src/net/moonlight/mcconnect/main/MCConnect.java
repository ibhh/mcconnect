/*
 * Copyright (c) 2015 Felix Beil
 * 
 * This file is part of McConnect.
 * 
 * McConnect is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * McConnect is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with McConnect. If not, see <http://www.gnu.org/licenses/>.
 */
package net.moonlight.mcconnect.main;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.UIManager;

import net.moonlight.mcconnect.gui.MainFrame;
import net.moonlight.mcconnect.gui.helper.Formatter;

/**
 * Handles the Minecraft rcon protocol.
 */
public class MCConnect
{
	public static final Logger logger = Logger.getLogger("mcconnect");
	public static final String LOG_LEVEL_GUI = "1000";
	public static final String LOG_LEVEL_SERVER = "1001";

	/**
	 * Used for update checks.
	 * @return The non visible internal program version.
	 */
	public static int getInternalVersion()
	{
		return 20;
	}

	/**
	 * The visible program version.
	 * @return The visible program version.
	 */
	public static String getVersion()
	{
		return "0.8";
	}

	/**
	 * Log a gui related message.
	 * @param msg The message to log.
	 */
	public static void logGui(String msg)
	{
		logger.log(Level.parse(LOG_LEVEL_GUI), msg);
	}

	/**
	 * Log a server related message.
	 * @param msg The message to log.
	 */
	public static void logServer(String msg)
	{
		logger.log(Level.parse(LOG_LEVEL_SERVER), msg);
	}

	public static void main(String[] args)
	{
		try
		{
			// Set LookAndFeel to system default
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch(Exception exception)
		{
			// or not ...
			exception.printStackTrace();
		}

		setupLogger();
		new MainFrame();
	}

	/**
	 * Setup the logger to output to the console in correct formatting.
	 */
	private static void setupLogger()
	{
		logger.setUseParentHandlers(false);

		Handler handlerConsole = new ConsoleHandler();
		handlerConsole.setFormatter(new Formatter());
		logger.addHandler(handlerConsole);
	}

	private Socket socket;

	private int requestID;

	/**
	 * Sends a command to the server.
	 * @param cmd The command to send.
	 * @return The answer of the server.<br />
	 *         Note:<br />
	 *         The answer might be empty, since not every command has an output, e.g. "say Hello, world!" outputs
	 *         "Hello, world!" to the in game chat, but doesn't return anything to the console, where as "list" is answered with
	 *         a list of all online players.
	 * @throws IOException If an I/O error occurs.
	 */
	public String command(String cmd) throws IOException
	{
		try
		{
			return send(2, cmd);
		}
		catch(IllegalAccessException exception)
		{
			System.err.println("Well, that is weired? o.O");
			exception.printStackTrace();
		}
		return "If you see this, something went wrong ...";
	}

	/**
	 * Starts a connection to a Minecraft server.
	 * @param host The host name or IP of the server.
	 * @param port The port to connect to (default for rcon: 25575)
	 * @throws UnknownHostException If the IP address of the host could not be determined.
	 * @throws IOException If an I/O error occurs when creating the socket.
	 */
	public void connect(String host, int port) throws UnknownHostException, IOException
	{
		disconnect();
		requestID = 0;
		socket = new Socket(host, port);
	}

	/**
	 * Disconnects form the minecraft server.
	 */
	public void disconnect()
	{
		if(socket != null)
		{
			try
			{
				socket.close();
			}
			catch(IOException exception)
			{
				exception.printStackTrace();
			}
			finally
			{
				socket = null;
			}
			logger.log(Level.parse(LOG_LEVEL_GUI), "Disconnected");
		}
	}

	/**
	 * Checks, whether there is an active connection to a server.
	 * @return <i>True</i>, if currently connected to a server.
	 */
	public boolean isConnected()
	{
		return socket != null;
	}

	/**
	 * Sends a login packet to the server.
	 * @param password The password to login with.
	 * @return Whether the login was successful.
	 * @throws IOException If an I/O error occurs.
	 */
	public boolean login(String password) throws IOException
	{
		try
		{
			send(3, password);
		}
		catch(IllegalAccessException exception)
		{
			return false;
		}
		return true;
	}

	/**
	 * Reads a little endian encoded integer.
	 * @param data The bytes to read the integer from.
	 * @param offset The offset in the data.
	 * @return An integer read from the data bytes.
	 */
	private int readInt(byte[] data, final int offset)
	{
		ByteBuffer byteBuffer = ByteBuffer.allocate(4);
		byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
		byteBuffer.put(data, offset, 4);
		byteBuffer.flip();
		return byteBuffer.getInt();
	}

	private String send(int pkType, String pkData) throws IOException, IllegalAccessException
	{
		if(socket == null)
		{
			throw new RuntimeException("Must connect before sending data!");
		}

		// Prepare headers
		ByteBuffer byteBuffer = ByteBuffer.allocate(8);
		byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
		byteBuffer.putInt(requestID); // RequestID
		byteBuffer.putInt(pkType); // Packet type

		byteBuffer.flip();
		byte[] header = byteBuffer.array();

		// Encode data
		byteBuffer = Charset.forName("UTF-8").encode(pkData);
		byteBuffer.flip();
		byte[] dataEncoded = byteBuffer.array();

		// Add padding (one null byte is added by the encode function)
		byteBuffer = ByteBuffer.allocate(dataEncoded.length + 1);
		byteBuffer.put(dataEncoded);
		byteBuffer.put((byte) 0); // Padding

		byteBuffer.flip();
		byte[] dataOut = byteBuffer.array();

		// Prepare packet length
		byteBuffer = ByteBuffer.allocate(4);
		byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
		byteBuffer.putInt(header.length + dataOut.length + 1);

		byteBuffer.flip();
		byte[] lenght = byteBuffer.array();

		// Build packet
		byteBuffer = ByteBuffer.allocate(5 + header.length + dataOut.length);
		byteBuffer.put(lenght);
		byteBuffer.put(header);
		byteBuffer.put(dataOut);
		byteBuffer.put((byte) 0);
		byteBuffer.flip();

		// Send packet
		OutputStream outputStream = socket.getOutputStream();
		outputStream.write(byteBuffer.array());
		outputStream.flush();

		// Wait for response
		InputStream inputStream = socket.getInputStream();
		while(inputStream.available() == 0)
			;

		// Read data
		String dataIn = "";
		while(inputStream.available() > 0)
		{
			// Read packet length
			byte[] inData = new byte[4];
			int bytesRead = socket.getInputStream().read(inData);
			if(bytesRead != 4)
			{
				throw new IOException("Could not read packet length!");
			}

			// Parse packet length
			int pkLength = readInt(inData, 0);

			// Read rest of packet
			inData = new byte[pkLength];
			bytesRead = socket.getInputStream().read(inData);
			if(bytesRead != pkLength)
			{
				throw new IOException("Could not read data!");
			}

			// Read header
			int inRequestID = readInt(inData, 0);
			int inPkType = readInt(inData, 4);

			// Check header and padding
			if(inRequestID == -1)
			{
				throw new IllegalAccessException("Login failed!");
			}
			if(inRequestID != requestID)
			{
				throw new IOException("Recived packet with wrong request id (id: " + inRequestID + ", should be: " + requestID + ")!");
			}
			if(inPkType < 0 || inPkType > 3)
			{
				throw new IOException("Recived unknown packet type (" + inPkType + ")!");
			}
			if(inData[pkLength - 2] != 0 || inData[pkLength - 1] != 0)
			{
				throw new IOException("Incorrect padding!");
			}

			// Read payload
			byteBuffer = ByteBuffer.allocate(pkLength - 10);
			byteBuffer.put(inData, 8, pkLength - 10);
			byteBuffer.flip();
			CharBuffer charBuffer = Charset.forName("UTF-8").decode(byteBuffer);
			StringBuilder sb = new StringBuilder();
			while(charBuffer.hasRemaining())
			{
				sb.append(charBuffer.get());
			}
			dataIn += sb.toString();
		}

		requestID++;
		return dataIn;
	}
}
